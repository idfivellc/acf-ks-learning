/**
 * Modules
 */
import { silcCoreInit } from "silc-core";
import { silcAccordionInit } from "silc-accordion";
import { silcNavInit } from "silc-nav";
import { silcOffcanvasInit } from "silc-offcanvas";
import focusWithin from "focus-within";

import {SilcCarouselInit} from "../components/carousel/carousel";
import Tablesaw from "../components/table/table";
import Modal from "../components/modal/modal";
import scrollyAnimation from "../components/scrolly/scrolly"

/**
 * Init
 */
focusWithin(document);
silcCoreInit();
silcAccordionInit();
silcNavInit();
silcOffcanvasInit();
SilcCarouselInit();
Tablesaw.init();
Modal.init();

function animationsInit() {
    let scrollyWrapper = document.querySelector(".scrolly") as HTMLElement;
    if (scrollyWrapper) {
        new scrollyAnimation(scrollyWrapper)
    }
}
animationsInit();
/**
 * Example VueJS app
 */
// import { vueApp } from "./vue-app";
// vueApp;


