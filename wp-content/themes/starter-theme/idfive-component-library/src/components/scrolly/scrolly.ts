import Vue from "vue";
export default class scrollyAnimations {
    public vueApp;
    private arrayofCircles;
    private windowHeight;
    private windowWidth;
    private scroll;
    public nbr_circles;
    public limitCircleSize;
    public cx;
    public cy;
    public lg_rad;
    public circlesSize;
    public totalScroll;
    public howFarIstheSCrollonTheSection;
    public sectionHeight;
    public wrapper;
    public arrayofSections;
    public arrayofDistances;
    public currentSection;




    public constructor(element) {
        this.wrapper = document.querySelector('.wrapper');
        this.circlesSize = 20;
        this.limitCircleSize = 100;
        this.lg_rad = (this.limitCircleSize / 2);
        this.cx = this.limitCircleSize / 2;
        this.cy = this.limitCircleSize / 2;
        this.nbr_circles = 5;
        this.arrayofSections = document.querySelectorAll('.section');
        this.arrayofDistances = [];
        this.currentSection = 0;

        this.arrayofCircles = [];

        this.vueApp = new Vue({
            el: element,
            data: {
                circles: this.arrayofCircles,
            },

        });
        this.createCircles(5);
        this.circlePosition(0);
        this.calculateSectionDistances();

        window.addEventListener("resize", () => {


        });
        window.addEventListener("scroll", () => {
            this.totalScroll = window.scrollY;
            let newScroll = this.calculateScrollPercentage(this.totalScroll);
            this.handleSwitch(this.currentSection, newScroll)


        });
    }

    public createCircles(number) {
        for (let i = 0; i < number; i++) {
            let structuredObject = {
                transform: ''
            };
            this.arrayofCircles.push(structuredObject);
        }

    }
    public circlePosition(newScroll) {

        for (let i = 0; i < this.arrayofCircles.length; i++) {
            var angle = i * 2 * Math.PI / this.nbr_circles;
            var x = (this.cx + Math.cos(angle) * this.lg_rad * (window.scrollY / 500)) - (this.circlesSize / 2);
            var y = (this.cy + Math.sin(angle) * this.lg_rad * (window.scrollY / 500)) - (this.circlesSize / 2);
            this.arrayofCircles[i].transform = "translate3d(" + x + "px, " + y + "px, 0)";

        }
    }

    public rotateContainer(windowScroll, sectionHeightInt) {
        this.wrapper.style.transform = "rotate(" + (windowScroll / (sectionHeightInt / 2)) * 360 + "deg)";
    }
    public calculateSectionDistances() {

        for (let i = 0; i < this.arrayofSections.length; i++) {
            let realDistancefromtop = this.getPureElementDistanceFromTop(this.arrayofSections[i])
            let heightOfSection = this.arrayofSections[i].clientHeight;

            let structuredObject = {
                distanceFromTop: realDistancefromtop,
                height: heightOfSection
            };
            this.arrayofDistances.push(structuredObject);
        }
    }
    public calculateScrollPercentage(windowScroll) {
        for (let i = 0; i < this.arrayofDistances.length; i++) {
            let completeHeight = this.arrayofDistances[i].height + this.arrayofDistances[i].distanceFromTop;
            // console.log(this.arrayofDistances[i].height+this.arrayofDistances[i].distanceFromTop+'section total height'+i)
            console.log(windowScroll)
            // console.log(completeHeight+"complete heigh section "+i)
            if (windowScroll >= this.arrayofDistances[i].distanceFromTop && windowScroll <= completeHeight) {
                console.log(i + " section");
                let newscroll = (completeHeight - windowScroll) / this.arrayofDistances[i].height;
                this.currentSection = i;
                console.log(newscroll + "newScroll")
                return newscroll
            }

        }

    }

    public getPureElementDistanceFromTop(element) {
        let distanceToReturn = 0;
        if (element.offsetParent) {
            do {
                distanceToReturn += element.offsetTop;
                element = element.offsetParent;
            } while (element);
        }
        if (distanceToReturn >= 0) {
            return distanceToReturn;
        } else {
            return 0;
        }
    }
    public handleSwitch(currentSection, newScroll) {
        switch (currentSection) {
            case 0:
                this.circlePosition(newScroll);
                break;
            case 1:
                this.rotateContainer(newScroll, currentSection);
                break;
            case 2:
            // this.rotateContainer(newScroll, currentSection);

        }
    }

}